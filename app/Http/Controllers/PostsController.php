<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Post;
use Illuminate\Http\Request;

class PostsController extends Controller
{

    public function getPostsCategories()
    {

        $rows = Category::getCategories();
        return Category::buildTreeForSelectMultiLevel($rows);

    }

    public function index()
    {
        $posts = Post::orderBy('id', 'desc')->paginate(15);
        $menu_tree = $this->getPostsCategories();


        return view('welcome', compact('posts','menu_tree'));

    }

    public function categoryPosts($id)
    {
        $category = Category::findOrFail($id);
        $posts = Post::categoryPosts($category->id);

        $menu_tree = $this->getPostsCategories();

        return view('categoryPosts')->with([
            'title' => $category->name,
            'posts' => $posts,
            'menu_tree' => $menu_tree
        ]);
    }




    public function show($id)
    {
        $post = Post::findOrFail($id);
        $menu_tree = $this->getPostsCategories();
        return view('show')->with([
            'title' => $post->name,
            'post' => $post,
            'menu_tree' => $menu_tree,

        ]);
    }
}
