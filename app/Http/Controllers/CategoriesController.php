<?php

namespace App\Http\Controllers;

use App\Models\Category;
use Illuminate\Http\Request;

class CategoriesController extends Controller
{
    public function getTopMenu()
    {
        $rows = Category::getCategories(App::getLocale());
        return Category::buildTreeForSelectMultiLevel($rows);


        // $menuitems = Menu::isLive()
        //     ->ofSort(['parent_id' => 'asc', 'sort_order' => 'asc'])
        //     ->get();


        // return buildTree($menuitems);
    }

}
