<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    use HasFactory;

    protected $table = 'posts';
    protected $fillable = ['name','desc','text','publish','user_id',];

    public function categories()
    {
        return $this->belongsToMany(Category::class, 'posts_categories', 'post_id', 'category_id');
    }

    public function addCategories($input)
    {
        if(empty($input['categories'])) return false;
        $ids = $input['categories'];
        if(isset($ids)){
            $this->categories()->sync($ids);
            return $this;
        }
        return false;
    }



    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public static function categoryPosts($cat_id)
    {
        return self::whereHas('categories', function ($query) use ($cat_id){
            $query->where('category_id', '=', $cat_id);
        })->orderBy('id', 'desc')
            ->paginate(15);
    }



}
