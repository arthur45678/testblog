<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;

class Category extends Model
{
    use HasFactory;

    protected $table = 'categories';
    protected $fillable = ['title','parent'];



    public function posts()
    {
        return $this->belongsToMany(Post::class, 'posts_categories', 'category_id','post_id');
    }


    public static function getCategories()
    {
        return (array) DB::select("SELECT * FROM categories ");
    }




    public static function buildTreeForSelectMultiLevel(Array $data, $parent = 0) {
        $tree = array();
        foreach ($data as $d) {
            $d = (array) $d;
            if ($d['parent'] == $parent) {
                $children = self::buildTreeForSelectMultiLevel($data, $d['id']);
                // set a trivial key
                if (!empty($children)) {
                    $d['_children'] = $children;
                }
                $tree[] = $d;
            }
        }
        return $tree;
    }


    public static function storeMenu($request)
    {
        $input = $request->all();

        return self::create($input);
    }

    public static function updateMenu($request, $id)
    {
        $input = $request->all();
        $item = self::findOrFail($id);
        return $item->update($input);
    }

    public static function deleteMenuItem($id)
    {

        self::findOrFail($id)->delete();

        $childeItems = self::whereIn('parent', [$id])->update(['parent' => 0]);
        return true;
    }

    public static function getTranslateTitles($category_id)
    {

        $sql = "SELECT * FROM categories WHERE parent = '$category_id'";
        $result = DB::select($sql);

        $str = '';
        foreach ($result as $item) {
            $str .= ' ' . $item->locale . ' -> ' .$item->title . ' |';
        }

        $str = rtrim($str, '|');
        return $str;
    }

}
