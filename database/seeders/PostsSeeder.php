<?php

namespace Database\Seeders;

use App\Models\Post;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class PostsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categories_ids = [1,2,3,4,5,6,7,8,9];
        \App\Models\Post::factory(100)->create();
        $posts = Post::all();
        foreach ($posts as $post) {
            $rand_keys = array_rand($categories_ids, 3);
            $post->categories()->attach($categories_ids[$rand_keys[0]]);
            $post->categories()->attach($categories_ids[$rand_keys[1]]);
            $post->categories()->attach($categories_ids[$rand_keys[2]]);
        }
    }
}
