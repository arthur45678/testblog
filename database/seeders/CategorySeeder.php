<?php

namespace Database\Seeders;

use App\Models\Category;
use Illuminate\Database\Seeder;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Category::insert([
            ['title' => 'Общество', 'parent' => 0],
            ['title' => 'городская жизнь', 'parent' => 1],
            ['title' => 'выборы', 'parent' => 1],
        ]);
        Category::insert([
            ['title' => 'День города', 'parent' => 0],
            ['title' => 'салюты', 'parent' => 4],
            ['title' => 'детская площадка', 'parent' => 4],
            ['title' => '0-3 года', 'parent' => 6],
            ['title' => '3-7 года', 'parent' => 6],

        ]);

        Category::insert([
            ['title' => 'Спорт', 'parent' => 0],
        ]);
    }
}
