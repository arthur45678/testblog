<?php

namespace Database\Factories;

use App\Models\Post;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class PostFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Post::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $text = $this->faker->text;
        return [
            'name' => $this->faker->title,
            'desc' => Str::limit($text,200),
            'text' => $text,
            'user_id' => 1 ,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ];
    }
}
