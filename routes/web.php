<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [\App\Http\Controllers\PostsController::class, 'index'])->name('posts');
Route::get('/categoryposts/{id}', [\App\Http\Controllers\PostsController::class, 'categoryPosts'])->name('categoryPosts');
Route::get('/show/{id}', [\App\Http\Controllers\PostsController::class, 'show'])->name('show');


/*Route::get('/', function () {
    return view('welcome');
});*/

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');


