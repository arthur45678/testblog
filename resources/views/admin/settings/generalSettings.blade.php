@extends('layouts.admin')

@section('styles')

@endsection

@section('sidebar')
    @include('admin.includes.sidebar')
@endsection

@section('content')
    @include('admin.includes.info-box')

    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">{{ $title }}</h6>
        </div>
        <div class="card-body">

            <form method="post" action="{{ route('admin.settings.update') }}" enctype="multipart/form-data">
                    @csrf
                    @foreach($data as $item)
                        {!! viewAdminSettingsItem($item) !!}
                    @endforeach

                <div class="form-group">
                    <button type="submit" class="btn btn-primary">Обновить</button>
                </div>
            </form>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        $(document).ready(function () {


        });
    </script>
@endsection
