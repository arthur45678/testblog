@extends('layouts.admin')

@section('styles')

@endsection

@section('sidebar')
    @include('admin.includes.sidebar')
@endsection

@section('content')
    @include('admin.includes.info-box')

    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">{{ $title }}</h6>
        </div>
        <div class="card-body">

            <form method="post" action="{{ route('admin.roles.store') }}">
                @csrf()
                <div class="form-group">
                    <label for="name">Name</label>
                    <input type="text" name="name" id="name" value="" class="form-control" placeholder="Name">
                </div>

                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>Permission:</strong>
                        <br/>
                        @foreach($permission as $value)
                            <label for="{{ $value->id }}">{{ $value->name }}</label>
                            <input type="checkbox" name="permission[]" value="{{ $value->id }}">
                            <br/>
                        @endforeach
                    </div>
                </div>

                <div class="form-group">
                    <button type="submit" class="btn btn-primary">Create</button>
                </div>
            </form>
        </div>
    </div>
@endsection

@section('scripts')

@endsection
