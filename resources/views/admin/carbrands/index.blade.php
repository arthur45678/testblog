@extends('layouts.admin')

@section('styles')

@endsection

@section('sidebar')
    @include('admin.includes.sidebar')
@endsection

@section('content')
    @include('admin.includes.info-box')

    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        @can('user-create')
            <div class="card-body">
                <p><a class="btn btn-primary" href="{{ route('admin.users.create') }}">Новый пользователь</a></p>
            </div><!--row-->
        @endcan

        {{--Search--}}
        <div class="card-header">Filter</div>
        <div class="card-body">
            <form action="?" method="GET">
                <div class="row">

                    <div class="col-sm-2">
                        <div class="form-group">
                            <label for="name" class="col-form-label">Name</label>
                            <input id="name" class="form-control" name="name" value="{{ request('name') }}">
                        </div>
                    </div>
                   
                    <div class="col-sm-2">
                        <div class="form-group">
                            <label class="col-form-label">&nbsp;</label><br />
                            <button type="submit" class="btn btn-primary">Search</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
        {{--Search end--}}

        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">{{ $title }}</h6>
        </div>
        <div class="card-body">
           <table class="table table-hover">
               <thead>
                   <tr>
                       <th>#ID</th>
                       <th>Название</th>
                       <th>Посмотреть</th>
                       <th>Действие</th>
                   </tr>
               </thead>
               <tbody>
               @foreach($posts as $post)
                   <tr>
                       <td>{{ $post->id }}</td>
                       <td>{{ $post->name }}</td>
                       <td><a href="{{ route('brand.show', $post->slug) }}"  class="badge badge-primary">На сайте</a></td>
                       <th>
                           <a href="{{ route('admin.carbrands.edit', [$post->id]) }}" class="badge badge-primary">Редактировать</a>
                           <form method="post" action="{{ route('admin.carbrands.destroy', [$post->id]) }}" class="d-inline">
                               @method('delete')
                               @csrf
                               <button type="submit" class="badge badge-danger" href="">Удалить</button>
                           </form>
                       </th>
                   </tr>
               @endforeach
               </tbody>
           </table>
            {{ $posts->links() }}
        </div>
    </div>
@endsection

@section('scripts')

@endsection
