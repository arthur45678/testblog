@extends('layouts.admin')

@section('styles')

@endsection

@section('sidebar')
    @include('admin.includes.sidebar')
@endsection

@section('content')
    @include('admin.includes.info-box')

    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">{{ $title }}</h6>
        </div>
        <div class="card-body">
           <form method="post" action="{{ route('admin.carmodels.update', $item->id) }}">
               @method('put')
               {{ csrf_field() }}

               <div class="row">
                   <div class="col-md-6">

                       <label for="carbrand_id">Производитель</label>
                       <select name="carbrand_id" id="carbrand_id" class="form-control select2">
                           @foreach($carbrands as $brand)
                               @if($brand->id == $item->brand->id )
                                   <option selected value="{{ $brand->id }}">{{ $brand->name }}</option>
                               @endif
                                   <option value="{{ $brand->id }}">{{ $brand->name }}</option>
                           @endforeach
                       </select>
                   </div>
                   <div class="col-md-6">
                       <label for="name">Название</label>
                       <input type="text" name="name" id="name" value="{{ $item->name }}" class="form-control">
                   </div>
               </div><!--row-->

               <br>
               <div class="row">
                   <div class="col-md-12">
                       <button type="submit" class="btn btn-primary">Обновить</button>
                   </div>
               </div><!--row-->
           </form>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        $(document).ready(function () {
            $(".select2").each(function () {
                $(this).select2({
                    theme: 'bootstrap4',
                    width: 'style',
                    placeholder: $(this).attr('placeholder'),
                    allowClear: Boolean($(this).data('allow-clear')),
                });
            });
        });
    </script>
@endsection
