@extends('layouts.admin')

@section('styles')

@endsection

@section('sidebar')
    @include('admin.includes.sidebar')
@endsection

@section('content')
    @include('admin.includes.info-box')

    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">{{ $title }}</h6>
        </div>
        <div class="card-body">
            <form method="post" action="{{ route('admin.users.update', [$user->id]) }}">
                @csrf
                @method('PATCH')
                <div class="form-group">
                    <label for="name">Name</label>
                    <input type="text" name="name" id="name" value="{{ isset($user->name) ? $user->name : '' }}" class="form-control">
                </div>


                <div class="form-group">
                    <label for="name">Email</label>
                    <input type="email" name="email" id="email" value="{{ isset($user->email) ? $user->email : '' }}" class="form-control">
                </div>

                <div class="form-group">
                    <label for="password">Password</label>
                    <input type="password" name="password" id="password" value="" class="form-control">
                </div>
                <div class="form-group">
                    <label for="confirm-password">Confirm Password</label>
                    <input type="confirm-password" name="confirm-password" id="confirm-password" value="" class="form-control">
                </div>


                <div class="form-group">
                    <label for="roles">Role:</label>
                    <select name="roles[]" id="roles" multiple class="form-control">
                        @foreach($roles as $role)

                            <option value="{{ $role }}" {{ in_array($role, $userRole) ? 'selected' : '' }}>{{ $role }}</option>

                        @endforeach
                    </select>
                </div>


                <div class="form-group">
                    <button type="submit" class="btn btn-primary">Update</button>
                </div>
            </form>
        </div>
    </div>
@endsection

@section('scripts')

@endsection
