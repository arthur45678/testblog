@extends('layouts.admin')

@section('styles')

@endsection

@section('sidebar')
    @include('admin.includes.sidebar')
@endsection

@section('content')
    @include('admin.includes.info-box')

    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">{{ $title }}</h6>
        </div>
        <div class="card-body">
            <form method="post" action="#" enctype="multipart/form-data" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="form-group">
                    <label for="name">Название</label>
                    <input type="text" name="name" id="name" value="{{ old('name') }}" class="form-control">
                </div>


                <div class="form-group">
                    <label for="image">Главное изображение записи</label>
                    <input type="file" name="image" id="image" class="form-control" >
                </div>

                {{--Add dynamic images fields--}}
                <div class="form-group after-add-more">
                    <div class="input-group-btn">
                        <button class="btn btn-success add-more" type="button"><i class="glyphicon glyphicon-plus"></i> Добавиь изображение</button>
                    </div>
                </div>

                <!-- Copy Fields -->
                <div class="form-group">
                    <div class="copy d-none">
                        <div class="control-group input-group" style="margin-top:10px">
                            <input name="images_create[]" id="images" type="file" class="form-control filestyle" data-buttonName="btn-primary">
                            <div class="input-group-btn">
                                <button class="btn btn-danger remove" type="button"><i class="glyphicon glyphicon-remove"></i> Remove</button>
                            </div>
                        </div>
                    </div>{{--Add dynamic images fields--}}
                </div>

                <div class="form-group">
                    <button type="submit" class="btn btn-primary">Добавить</button>
                </div>


            </form>
        </div>
    </div>
@endsection

@section('scripts')
<script>
$(document).ready(function () {
    //Add dynamic images
    $(".add-more").click(function(){
        var html = $(".copy").html();
        $(".after-add-more").after(html);
    });

    $("body").on("click",".remove",function(){
        $(this).parents(".control-group").remove();
    });

    //Add dynamic images end

    /*Select2*/
    $(".select2").each(function () {
        $(this).select2({
            theme: 'bootstrap4',
            width: 'style',
            placeholder: $(this).attr('placeholder'),
            allowClear: Boolean($(this).data('allow-clear')),
        });
    });
    /*Select2 end*/
});
</script>
@endsection
