@extends('layouts.admin')

@section('styles')

@endsection

@section('sidebar')
    @include('admin.includes.sidebar')
@endsection

@section('content')
    @include('admin.includes.info-box')

    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">{{ $title }}</h6>
        </div>
        <div class="card-body">
           <form method="post" action="#" enctype="multipart/form-data">
               @method('put')
               @csrf
               <div class="form-group">
                   <label for="name">Название</label>
                   <input type="text" name="name" id="name" value="{{ $item->name }}" class="form-control">
               </div>

               {{--Post image--}}
               <div class="row">
                   <div class="col-md-6">
                       <div class="form-group">
                           <label for="image">Главное изображение продукта</label>
                           <input type="file" name="image" id="image" class="form-control" >
                       </div>
                   </div>
                   <div class="col-md-4">
                       <div class="form-group">
                           @if($item->image)
                               <img style="max-width: 118px" src="{{ $imagePath }}/{{ $item->image }}" alt="">
                           @else
                               {{--no-image.jpg--}}
                               <img style="max-width: 118px" src="{{ $imagesServe }}/page_no_image.jpg" alt="">
                           @endif
                       </div>
                   </div>

                   @if(isset($item->image))
                       <div class="col-md-2">
                           <label for="delete_image">Удалить изображение</label>
                           <input type="checkbox" id="delete_image" name="delete_image[{{ $item->image }}]">
                       </div>
                   @endif
               </div><!--row-->
               {{--End Post image--}}


               <div class="form-group">
                   <button type="submit" class="btn btn-primary">Обновить</button>
               </div>
           </form>
        </div>
    </div>
@endsection

@section('scripts')
<script>
$(document).ready(function () {
    //Add dynamic images
    $(".add-more").click(function(){
        var html = $(".copy").html();
        $(".after-add-more").after(html);
    });

    $("body").on("click",".remove",function(){
        $(this).parents(".control-group").remove();
    });

    //Add dynamic images end
    
    /*Select2*/
    $(".select2").each(function () {
        $(this).select2({
            theme: 'bootstrap4',
            width: 'style',
            placeholder: $(this).attr('placeholder'),
            allowClear: Boolean($(this).data('allow-clear')),
        });
    });
    /*Select2 end*/
});
</script>
@endsection
