<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

    <!-- Sidebar - Brand -->
    <a class="sidebar-brand d-flex align-items-center justify-content-center" href="/">
        <div class="sidebar-brand-icon rotate-n-15">
        </div>
        <div class="sidebar-brand-text mx-3">NKparts</div>
    </a>

    <!-- Divider -->
    <hr class="sidebar-divider my-0">

    <!-- Nav Item - Dashboard -->
    <li class="nav-item">
        <a class="nav-link" href="{{ route('admin.home') }}">
            <i class="fas fa-fw fa-tachometer-alt"></i>
            <span>Главная страница</span></a>
    </li>

    @can('settings-edit')
        <li class="nav-item active">
            <a class="nav-link" href="{{ route('admin.settings.generalSettings') }}">
                <i class="fas fa-fw fa-table"></i>
                <span>Основные настройки</span></a>
        </li>
    @endcan


    <hr class="sidebar-divider">
    @can('posts-list')
    <!-- Heading -->
        <div class="sidebar-heading">
            Статьи
        </div>
    @endcan


    @can('categories-list')
        <li class="nav-item">
            <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#categories" aria-expanded="true" aria-controls="categories">
                <i class="fas fa-fw fa-cog"></i>
                <span>Категрий для статьи</span>
            </a>
            <div id="categories" class="collapse" aria-labelledby="categories" data-parent="#accordionSidebar">
                <div class="bg-white py-2 collapse-inner rounded">
                    <a class="collapse-item" href="{{ route('admin.categories.index') }}">Все категории</a>
                    @can('posts-create')
                        <a class="collapse-item" href="{{ route('admin.categories.create') }}">Добавить категорий</a>
                    @endcan
                </div>
            </div>
        </li>
    @endcan


    @can('tags-list')
        <li class="nav-item">
            <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#tags" aria-expanded="true" aria-controls="tags">
                <i class="fas fa-fw fa-cog"></i>
                <span>Теги для статьи</span>
            </a>
            <div id="tags" class="collapse" aria-labelledby="tags" data-parent="#accordionSidebar">
                <div class="bg-white py-2 collapse-inner rounded">
                    <a class="collapse-item" href="{{ route('admin.tags.index') }}">Все теги</a>
                    @can('posts-create')
                        <a class="collapse-item" href="{{ route('admin.tags.create') }}">Новый тег</a>
                    @endcan
                </div>
            </div>
        </li>
    @endcan

    @can('posts-list')
        <li class="nav-item">
            <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#posts" aria-expanded="true" aria-controls="posts">
                <i class="fas fa-fw fa-cog"></i>
                <span>Статьи</span>
            </a>
            <div id="posts" class="collapse" aria-labelledby="posts" data-parent="#accordionSidebar">
                <div class="bg-white py-2 collapse-inner rounded">
                    <a class="collapse-item" href="{{ route('admin.posts.index') }}">Все статьи</a>
                    @can('posts-create')
                        <a class="collapse-item" href="{{ route('admin.posts.create') }}">Дбавить статью</a>
                    @endcan
                </div>
            </div>
        </li>
    @endcan



    <!-- Divider -->
    <hr class="sidebar-divider">
    @can('products-list')
        <!-- Heading -->
        <div class="sidebar-heading">
            Продукты
        </div>
    @endcan


    @can('menus-list')
        <li class="nav-item">
            <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#menus" aria-expanded="true" aria-controls="menus">
                <i class="fas fa-fw fa-cog"></i>
                <span>Верхнее меню</span>
            </a>
            <div id="menus" class="collapse" aria-labelledby="menus" data-parent="#accordionSidebar">
                <div class="bg-white py-2 collapse-inner rounded">
                    <a class="collapse-item" href="{{ route('admin.menus.index') }}">Меню</a>
                    @can('menus-create')
                        <a class="collapse-item" href="{{ route('admin.menus.create') }}">Новый пункт в меню</a>
                    @endcan
                </div>
            </div>
        </li>
    @endcan

    @can('orders-list')
        <li class="nav-item">
            <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#orders" aria-expanded="true" aria-controls="orders">
                <i class="fas fa-fw fa-cog"></i>
                <span>Заказы</span>
            </a>
          <div id="orders" class="collapse" aria-labelledby="orders" data-parent="#accordionSidebar">
                <div class="bg-white py-2 collapse-inner rounded">
                    <a class="collapse-item" href="{{ route('admin.orders.index') }}">Все заказы</a>
                 {{--   @can('orders-create')

                        <a class="collapse-item" href="{{ route('admin.orders.orderAddProduct') }}">Новый заказ</a>
                    @endcan--}}
                </div>
            </div>
        </li>
    @endcan

    @can('products-list')
        <li class="nav-item">
            <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#products" aria-expanded="true" aria-controls="products">
                <i class="fas fa-fw fa-cog"></i>
                <span>Продукты</span>
            </a>
            <div id="products" class="collapse" aria-labelledby="products" data-parent="#accordionSidebar">
                <div class="bg-white py-2 collapse-inner rounded">
                    <a class="collapse-item" href="{{ route('admin.products.index') }}">Все продукты</a>
                    @can('products-create')
                        <a class="collapse-item" href="{{ route('admin.products.create') }}">Добавить продукт</a>
                    @endcan
                </div>
            </div>
        </li>
    @endcan


  {{--  @can('coupons-list')
        <li class="nav-item">
            <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#coupons" aria-expanded="true" aria-controls="coupons">
                <i class="fas fa-fw fa-cog"></i>
                <span>Купоны</span>
            </a>
            <div id="coupons" class="collapse" aria-labelledby="coupons" data-parent="#accordionSidebar">
                <div class="bg-white py-2 collapse-inner rounded">
                    <a class="collapse-item" href="{{ route('admin.coupons.index') }}">Все купоны</a>
                    @can('coupons-create')
                        <a class="collapse-item" href="{{ route('admin.coupons.create') }}">Добавить купон</a>
                    @endcan
                </div>
            </div>
        </li>
    @endcan--}}


    @can('carbrands-list')
        <li class="nav-item">
            <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#carbrands" aria-expanded="true" aria-controls="carbrands">
                <i class="fas fa-fw fa-cog"></i>
                <span>Производители</span>
            </a>
            <div id="carbrands" class="collapse" aria-labelledby="carbrands" data-parent="#accordionSidebar">
                <div class="bg-white py-2 collapse-inner rounded">
                    <a class="collapse-item" href="{{ route('admin.carbrands.index') }}">Все производители</a>
                    @can('carbrands-create')
                        <a class="collapse-item" href="{{ route('admin.carbrands.create') }}">Добавить производителья</a>
                    @endcan
                </div>
            </div>
        </li>
    @endcan


    @can('carmodels-list')
        <li class="nav-item">
            <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#carmodels" aria-expanded="true" aria-controls="carmodels">
                <i class="fas fa-fw fa-cog"></i>
                <span>Модели машин</span>
            </a>
            <div id="carmodels" class="collapse" aria-labelledby="carmodels" data-parent="#accordionSidebar">
                <div class="bg-white py-2 collapse-inner rounded">
                    <a class="collapse-item" href="{{ route('admin.carmodels.index') }}">Все модели</a>
                    @can('carmodels-create')
                        <a class="collapse-item" href="{{ route('admin.carmodels.create') }}">Добавить модель</a>
                    @endcan
                </div>
            </div>
        </li>
    @endcan

    @can('cargenerations-list')
        <li class="nav-item">
            <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#cargenerations" aria-expanded="true" aria-controls="cargenerations">
                <i class="fas fa-fw fa-cog"></i>
                <span>Поколение машин</span>
            </a>
            <div id="cargenerations" class="collapse" aria-labelledby="cargenerations" data-parent="#accordionSidebar">
                <div class="bg-white py-2 collapse-inner rounded">
                    <a class="collapse-item" href="{{ route('admin.cargenerations.index') }}">Все поколении</a>
                    @can('cargenerations-create')
                        <a class="collapse-item" href="{{ route('admin.cargenerations.create') }}">Добавить поколение</a>
                    @endcan
                </div>
            </div>
        </li>
    @endcan

    @can('detailtypes-list')
        <li class="nav-item">
            <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#detailtypes" aria-expanded="true" aria-controls="detailtypes">
                <i class="fas fa-fw fa-cog"></i>
                <span>Типы деталией</span>
            </a>
            <div id="detailtypes" class="collapse" aria-labelledby="detailtypes" data-parent="#accordionSidebar">
                <div class="bg-white py-2 collapse-inner rounded">
                    <a class="collapse-item" href="{{ route('admin.detailtypes.index') }}">Типы деталией</a>
                    @can('detailtypes-create')
                        <a class="collapse-item" href="{{ route('admin.detailtypes.create') }}">Добавить тип</a>
                    @endcan
                </div>
            </div>
        </li>
    @endcan

<!-- Heading -->
    <div class="sidebar-heading">
        Пользователи
    </div>


    @can('role-list')
        <li class="nav-item">
            <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#roles" aria-expanded="true" aria-controls="roles">
                <i class="fas fa-fw fa-cog"></i>
                <span>Роли</span>
            </a>
            <div id="roles" class="collapse" aria-labelledby="roles" data-parent="#roles">
                <div class="bg-white py-2 collapse-inner rounded">
                    <a class="collapse-item" href="{{ route('admin.roles.index') }}">Все роли</a>
                    @can('role-create')
                        <a class="collapse-item" href="{{ route('admin.roles.create') }}">Добавиьт роль</a>
                    @endcan
                </div>
            </div>
        </li>
    @endcan

    @can('user-list')
        <li class="nav-item">
            <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#users" aria-expanded="true" aria-controls="users">
                <i class="fas fa-fw fa-cog"></i>
                <span>Пользователи</span>
            </a>
            <div id="users" class="collapse" aria-labelledby="user" data-parent="#accordionSidebar">
                <div class="bg-white py-2 collapse-inner rounded">
                    <a class="collapse-item" href="{{ route('admin.users.index') }}">Все пользователи</a>
                    @can('user-create')
                        <a class="collapse-item" href="{{ route('admin.users.create') }}">Новый пользователь</a>
                    @endcan
                </div>
            </div>
        </li>
@endcan





<!-- Divider -->
    <hr class="sidebar-divider">




    <!-- Divider -->
    <hr class="sidebar-divider d-none d-md-block">

    <!-- Sidebar Toggler (Sidebar) -->
    <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
    </div>

</ul>
