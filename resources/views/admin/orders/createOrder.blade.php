@extends('layouts.admin')

@section('styles')

@endsection

@section('sidebar')
    @include('admin.includes.sidebar')
@endsection

@section('content')
    @include('admin.includes.info-box')

    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">{{ $title }}</h6>
        </div>
        <div class="card-body">
            <form method="post" action="{{ route('admin.orders.store') }}">
                {{ csrf_field() }}

                <div class="row">
                    <div class="col-md-12">
                        <table class="table table-hover">
                            <thead>
                            <tr>
                                <th>Название продукта</th>
                                <th>Инвентарный номер</th>
                                <th>Оригинальный номер</th>
                                <th>Цена</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td>{{ $product->name  }}</td>
                                <td>{{ $product->inventar_number }}</td>
                                <td>{{ $product->original_number }}</td>
                                <td>{{ $product->price }}</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>

                </div><!--row-->

                <div class="row">


                    <div class="col-md-6">
                        <input type="hidden" name="product_id" value="{{ $product->id }}">
                        <div class="form-group">
                            <h3>Заполните данные</h3>
                        </div>


                        <div class="form-group">
                            <label for="quantity">Количество</label>
                            <input type="text" class="form-control" id="quantity" name="quantity" value="{{ old('quantity') }}" >
                        </div>

                        <div class="form-group">
                            <label for="name">Имя</label>
                            <input type="text" class="form-control" id="name" name="name" value="{{ old('name') }}" >
                        </div>

                        <div class="form-group">
                            <label for="billing_phone_1">Ваш телефон</label>
                            <input type="tel" class="form-control" id="billing_phone_1" name="billing_phone_1" value="{{ old('billing_phone_1') }}">
                        </div>

                        <div class="form-group">
                            <label for="billing_phone_2">Телефон</label>
                            <input type="text" name="billing_phone_2" id="billing_phone_2" value="{{ old('billing_phone_2') }}" class="form-control">
                        </div>


                        <div class="form-group">
                            <label for="email">Email</label>
                            <input type="email" class="form-control" id="email" name="email" value="{{ old('email') }}" >
                        </div>





                      {{--  <div class="form-group">
                            <label for="postalcode">Почтовый индекс</label>
                            <input type="text" class="form-control" id="postalcode" name="postalcode" value="{{ old('postalcode') }}">
                        </div>--}}
                        <div class="form-group">
                            <label for="address">Адрес</label>
                            <input type="text" class="form-control" id="address" name="address" value="{{ old('address') }}" >
                        </div>

                        <div class="half-form">
                            <div class="form-group">
                                <label for="city">Город</label>
                                <input type="text" class="form-control" id="city" name="city" value="{{ old('city') }}" >
                            </div>
                         {{--   <div class="form-group">
                                <label for="province">Область</label>
                                <input type="text" class="form-control" id="province" name="province" value="{{ old('province') }}" >
                            </div>--}}
                        </div> <!-- end half-form -->

                        <div class="form-group">
                            <label for="comment">Примечание:</label>
                            <textarea name="comment" id="comment" cols="30" rows="5" class="form-control"></textarea>
                        </div>

                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <h3>Доставка</h3>
                        </div>

                        <h6>Тип доставки</h6>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="shipping_type" id="shipping_type_pickup" value="option1" checked>
                            <label class="form-check-label" for="shipping_type_pickup">
                                Самовывоз
                            </label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="shipping_type" id="shipping_type_transport_company" value="option2">
                            <label class="form-check-label" for="shipping_type_transport_company">
                                Доставка транс. компанией
                            </label>
                        </div>


                        <div class="form-group">
                            <button type="submit" class="btn btn-primary">Добавить заказ</button>
                        </div>

                    </div>
                </div><!--row-->


            </form>
        </div>
    </div>
@endsection

@section('scripts')
<script>
$(document).ready(function () {
    // Show and hide registered users fields Check the radio button value.
    $('#transe_company_wrap').hide('200');
    setInterval(function(){
        //console.log($('#shipping_type_pickup').is(':checked'));
        if($('#shipping_type_pickup').is(':checked')){
            $('#transe_company_wrap').hide('200');
        }

        if($('#shipping_type_transport_company').is(':checked')){
            $('#transe_company_wrap').show('200');
        }

    }, 200);


});
</script>
@endsection
