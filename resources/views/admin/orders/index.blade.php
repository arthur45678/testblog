@extends('layouts.admin')

@section('styles')

@endsection

@section('sidebar')
    @include('admin.includes.sidebar')
@endsection

@section('content')
    @include('admin.includes.info-box')

    <!-- DataTales Example -->
    <div class="card shadow mb-4">

        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">{{ $title }}</h6>
        </div>
        <div class="card-body">
            <table class="table table-hover">
                <thead>
                <tr>
                    <th>#ID заказа</th>
                    <th>Пользователь</th>
                    <th>Сумма</th>
                    <th>Тип доставки</th>
                    <th>Статус</th>
                    <th>Дата</th>
                    <th>Действие</th>
                </tr>
                </thead>
                <tbody>
                @foreach($orders as $order)
                    <tr>
                        <td>{{ $order->id }}</td>
                        <td>{{ $order->user->name }}</td>
                        <td>{{ presentPrice($order->billing_total) }}</td>
                        <td>{{ $order->shipping_type }}</td>
                        <td>{{ showShippedSatus($order) }}</td>
                        <td>{{ presentDate($order->created_at) }}</td>
                        <td>
                            <a href="{{ route('admin.orders.edit', $order->id) }}" class="badge badge-primary">Редактировать</a>
                            <form method="post" action="{{ route('admin.orders.destroy', $order->id) }}" class="d-inline">
                                @method('delete')
                                @csrf
                                <button type="submit" class="badge badge-danger">Удалить</button>
                            </form>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>

            {{ $orders->links() }}
        </div>
    </div>
@endsection

@section('scripts')

@endsection
