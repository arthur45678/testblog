@extends('layouts.admin')

@section('styles')

@endsection

@section('sidebar')
    @include('admin.includes.sidebar')
@endsection

@section('content')
    @include('admin.includes.info-box')

    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">{{ $title }}</h6>
        </div>
        <div class="card-body">
            <form method="post" action="{{ route('admin.orders.store') }}">
                {{ csrf_field() }}

                <div class="row">

                    <div class="col-md-6">
                        <div class="form-group">
                            <h3>Данные покупателя</h3>
                        </div>
                        <div class="form-group">
                            <label for="">Электронный адрес</label>
                            <input type="text" name="" id="" value="{{ old('') }}" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="billing_phone_1">Телефон 1</label>
                            <input type="text" name="billing_phone_1" id="billing_phone_1" value="{{ old('billing_phone_1') }}" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="billing_phone_2">Телефон 2</label>
                            <input type="text" name="billing_phone_2" id="billing_phone_2" value="{{ old('billing_phone_2') }}" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="">Ф.И.О. покупателя:</label>
                            <input type="text" name="" id="" value="{{ old('') }}" class="form-control">
                        </div>

                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <h3>Доставка</h3>
                        </div>

                        <h6>Тип доставки</h6>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="shipping_type" id="shipping_type_pickup" value="option1" checked>
                            <label class="form-check-label" for="shipping_type_pickup">
                                Самовывоз
                            </label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="shipping_type" id="shipping_type_transport_company" value="option2">
                            <label class="form-check-label" for="shipping_type_transport_company">
                                Доставка транс. компанией
                            </label>
                        </div>



                    </div>
                </div><!--row-->

                /*Test*/
                Old
                /*Testend*/

                <div class="form-group">
                    <label for="billing_email">Billing Email</label>
                    <input required type="billing_email" name="billing_email" id="billing_email" value="{{ old('billing_email') }}" class="form-control">
                </div>

                <div class="form-group">
                    <label for="billing_name">Billing Name</label>
                    <input required type="billing_name" name=billing_name"" id="billing_name" value="{{ old('billing_name') }}" class="form-control">
                </div>
                <div class="form-group">
                    <label for="billing_city">Billing City</label>
                    <input type="billing_city" name="billing_city" id="billing_city" value="{{ old('billing_city') }}" class="form-control">
                </div>
              {{--  <div class="form-group">
                    <label for="billing_province">Billing Province</label>
                    <input type="billing_province" name="billing_province" id="billing_province" value="{{ old('billing_province') }}" class="form-control">
                </div>--}}
             {{--   <div class="form-group">
                    <label for="billing_postalcode">Billing Postalcode</label>
                    <input type="billing_postalcode" name="billing_postalcode" id="billing_postalcode" value="{{ old('billing_postalcode') }}" class="form-control">
                </div>--}}
                <div class="form-group">
                    <label for="billing_phone_1">Phone 1</label>
                    <input type="billing_phone_1" name="billing_phone_1" id="billing_phone_1" value="{{ old('billing_phone_1') }}" class="form-control">
                </div>

                <div class="form-group">
                    <label for="billing_phone_2">Phone 2</label>
                    <input type="billing_phone_2" name="billing_phone_2" id="billing_phone_2" value="{{ old('billing_phone_2') }}" class="form-control">
                </div>

                <div class="form-group">
                    <label for="billing_name_on_card">Billing Name On Card</label>
                    <input type="billing_name_on_card" name="billing_name_on_card" id="billing_name_on_card" value="{{ old('billing_name_on_card') }}" class="form-control">
                </div>

                <div class="form-group">
                    <button type="submit" class="btn btn-primary">Добавить</button>
                </div>
                /*Test*/
                end Old
                /*Testend*/

            </form>
        </div>
    </div>
@endsection

@section('scripts')
<script>
$(document).ready(function () {
    // Check the radio button value.
    setInterval(function(){
        //console.log($('#shipping_type_pickup').is(':checked'));
        if($('#shipping_type_pickup').is(':checked')){
          //  $('#shipping_address_wrap').hide();
        }

        if($('#shipping_type_transport_company').is(':checked')){
          //  $('#shipping_address_wrap').show();
        }

    }, 200);


});
</script>
@endsection
