@extends('layouts.admin')

@section('styles')

@endsection

@section('sidebar')
    @include('admin.includes.sidebar')
@endsection

@section('content')
    @include('admin.includes.info-box')

    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">{{ $title }}</h6>
        </div>

        <div class="card-body">
           <form method="post" action="{{ route('admin.orders.update', $order->id) }}">
               @method('put')
               @csrf
               <table class="table table-hover">
                   <thead>
                       <tr>
                           <th>Email</th>
                           <th>Имя</th>
                           <th>Город</th>
                           <th>Адрес</th>
                           <th>Тел.</th>
                           <th>Тел.</th>
                       </tr>
                   </thead>
                   <tbody>
                       <tr>
                           <td>{{ $order->billing_email }}</td>
                           <td>{{ $order->billing_name }}</td>
                           <td>{{ $order->billing_city }}</td>
                           <td>{{ $order->billing_address }}</td>
                           <td>{{ $order->billing_phone_1 }}</td>
                           <td>{{ $order->billing_phone_2 }}</td>
                       </tr>

                   </tbody>
               </table>

               <table class="table table-hover">
                   <thead>
                       <tr>
                           <td class="text-center">ID заказа
                               <br>
                               {{ $order->id }}
                           </td>
                           <td class="text-center">Изображения</td>
                           <td class="text-center">Заказ добавлен
                               <br>
                               {{ presentDate($order->created_at) }}
                           </td>

                            <td class="text-center">Тип доставки
                               <br>
                                <strong>{{ $order->shipping_type }}</strong>
                           </td>


                           <td class="text-center">Итого
                               <br>
                               {{ presentPrice($order->billing_total) }}
                           </td>

                           <td class="text-center">Статус
                               <select class="form-control" name="shipped">
                                   <option value="{{ \App\Models\Order::STATUS_WAIT }}" {{ $order->shipped == \App\Models\Order::STATUS_WAIT ? 'selected' : '' }}>{{ Lang::get('orders.order_shipping_status_WAIT') }}</option>
                                   <option value="{{ \App\Models\Order::STATUS_IN_YEREVAN }}" {{ $order->shipped == \App\Models\Order::STATUS_IN_YEREVAN ? 'selected' : '' }}>{{ Lang::get('orders.order_shipping_status_IN_YEREVAN') }}</option>
                                   <option value="{{ \App\Models\Order::STATUS_ACTIVE }}" {{ $order->shipped == \App\Models\Order::STATUS_ACTIVE ? 'selected' : '' }}>{{ Lang::get('orders.order_shipping_status_ACTIVE') }}</option>
                                   <option value="{{ \App\Models\Order::STATUS_CANCELED }}" {{ $order->shipped == \App\Models\Order::STATUS_CANCELED ? 'selected' : '' }}>{{ Lang::get('orders.order_shipping_status_CANCELED') }}</option>

                               </select>


                           </td>
                       </tr>
                   </thead>
                   <tbody>
                   @foreach ($order->products as $product)
                       <tr>
                           <td class="text-center">
                               <a href="{{ route('shop.show', $product->slug) }}"><img width="85" class="img-thumbnail" title="{{ $product->name }}" alt="{{ $product->name }}" src="{{ getProductAvatar($product) }}">
                               </a>
                           </td>
                           <td class="text-left">

                               Название: <a href="{{ route('shop.show', $product->slug) }}">{{ $product->name }}</a>
                               <br>
                               Цена: {{ $product->presentPrice() }}
                               <br>
                               Количество: {{ $product->pivot->quantity }}

                               <br>
                               Удаленный сайт: {{ $product->siteparse_url }}

                               <br>
                               ID товара: {{ $product->inventar_number }}
                               <br>
                               Оригинальный номер: {{ $product->original_number }}

                           </td>

                       </tr>
                   @endforeach
                   </tbody>
               </table>


               <strong>Примечание:</strong><p>&nbsp;{{ $order->comment }}</p>

               <div class="form-group">
                   <button type="submit" class="btn btn-primary">Обновить</button>
               </div>
           </form>
        </div>
    </div>
@endsection

@section('scripts')
<script>
$(document).ready(function () {
    //Add dynamic images
    $(".add-more").click(function(){
        var html = $(".copy").html();
        $(".after-add-more").after(html);
    });

    $("body").on("click",".remove",function(){
        $(this).parents(".control-group").remove();
    });

    //Add dynamic images end

    /*Select2*/
    $(".select2").each(function () {
        $(this).select2({
            theme: 'bootstrap4',
            width: 'style',
            placeholder: $(this).attr('placeholder'),
            allowClear: Boolean($(this).data('allow-clear')),
        });
    });
    /*Select2 end*/
});
</script>
@endsection
