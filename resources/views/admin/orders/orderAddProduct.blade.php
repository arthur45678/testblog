@extends('layouts.admin')

@section('styles')

@endsection

@section('sidebar')
    @include('admin.includes.sidebar')
@endsection

@section('content')
    @include('admin.includes.info-box')

    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">{{ $title }}</h6>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-md-12">
                    <h3>Писк продукта для добавления в заказ</h3>
                    <form method="get" action="{{ route('admin.orders.orderAddProduct') }}">
                        @csrf
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="searchText">Введите номер или название продукта</label>
                                <input type="text" name="searchText" id="searchText" value="" class="form-control">
                            </div>

                            <div class="form-group">
                                <button type="submit" class="btn btn-primary">Поиск</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div><!--row-->


            @if(isset($searchResult) && count($searchResult) > 0)
                <div class="row">
                    <div class="col-md-12">
                        <h3>Результаты поиска</h3>
                        <table class="table table-hover">
                            <thead>
                            <tr>
                                <th>Название продукта</th>
                                <th>Инвентарный номер</th>
                                <th>Оригинальный номер</th>
                                <th>Цена</th>
                                <th>Добавиьт в заказ</th>

                            </tr>
                            </thead>
                            <tbody>
                            @foreach($searchResult as $item)
                                <?php $product = \App\Models\Product::find($item->id) ?>
                                <tr>
                                    <td>{{ $product->name  }}</td>
                                    <td>{{ $product->inventar_number }}</td>
                                    <td>{{ $product->original_number }}</td>
                                    <td>{{ $product->price }}</td>
                                    <td>
                                        <form method="get" action="{{ route('admin.orders.createOrder', $product->id) }}">
                                            <div class="form-group">
                                                <button type="submit" class="btn btn-primary">Заказать</button>
                                            </div>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        {{ $searchResult->appends(Request::except('page'))->links() }}
                    </div>
                </div><!--row-->
            @endif


        </div>
    </div>
@endsection

@section('scripts')
<script>
$(document).ready(function () {
    // Check the radio button value.
    setInterval(function(){
        //console.log($('#shipping_type_pickup').is(':checked'));
        if($('#shipping_type_pickup').is(':checked')){
          //  $('#shipping_address_wrap').hide();
        }

        if($('#shipping_type_transport_company').is(':checked')){
          //  $('#shipping_address_wrap').show();
        }

    }, 200);


});
</script>
@endsection
