@extends('layouts.admin')

@section('styles')

@endsection

@section('sidebar')
    @include('admin.includes.sidebar')
@endsection

@section('content')
    @include('admin.includes.info-box')

    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">{{ $title }}</h6>
        </div>
        <div class="card-body">
            <form method="post" action="{{ route('admin.posts.store') }}" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="form-group">
                    <label for="name">Название</label>
                    <input type="text" name="name" id="name" value="{{ old('name') }}" class="form-control">
                </div>

                <div class="form-group">
                    <label for="image">Изображение статьи</label>
                    <input type="file" name="image" id="image" class="form-control" >
                </div>

                <div class="form-group">
                    <label for="desc">Краткое описание</label>
                    <textarea name="desc" id="desc" cols="30" rows="5" class="form-control">{{ old('desc') }}</textarea>
                </div>

                <label for="text">Текст</label>
                <textarea id="text" name="text">{{ old('text') }}</textarea>
                <script>
                    var options = {
                        filebrowserImageBrowseUrl: '/laravel-filemanager?type=Images',
                        filebrowserImageUploadUrl: '/laravel-filemanager/upload?type=Images&_token=',
                        filebrowserBrowseUrl: '/laravel-filemanager?type=Files',
                        filebrowserUploadUrl: '/laravel-filemanager/upload?type=Files&_token='
                    };

                    CKEDITOR.replace("text", options);
                </script>

                <div class="form-group">
                    <label for="tags">Теги</label>
                    <select name="tags[]" id="tags" class="select2"  multiple="multiple">
                        @foreach($tags as $item)
                            <option value="{{ $item->id }}">{{ $item->name }}</option>
                        @endforeach
                    </select>
                </div>



                <div class="form-group">
                    <label for="categories">Категории</label>
                    <select name="categories[]" id="categories" class="select2"   multiple="multiple">
                        {{ selectNested($categories) }}
                    </select>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <label for="meta_desc">Мета описание</label>
                        <textarea name="meta_desc" id="meta_desc" cols="15" rows="5" class="form-control">{{ $item->meta_desc }}</textarea>
                    </div>
                    <div class="col-md-6">
                        <label for="keywords">Клчевые слова</label>
                        <textarea name="keywords" id="keywords" cols="15" rows="5" class="form-control">{{ old('keywords') }}</textarea>
                    </div>
                </div><!--row-->
                <br>

                <div class="form-group">
                    <button type="submit" class="btn btn-primary">Добавить</button>
                </div>
            </form>
        </div>
    </div>
@endsection

@section('scripts')
<script>
$(document).ready(function () {

    /*Select2*/
    $(".select2").each(function () {
        $(this).select2({
            theme: 'bootstrap4',
            width: 'style',
            placeholder: $(this).attr('placeholder'),
            allowClear: Boolean($(this).data('allow-clear')),
        });
    });
    /*Select2 end*/
});
</script>
@endsection
