@extends('layouts.admin')

@section('styles')

@endsection

@section('sidebar')
    @include('admin.includes.sidebar')
@endsection

@section('content')
    @include('admin.includes.info-box')

    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">{{ $title }}</h6>
        </div>
        <div class="card-body">
           <form method="post" action="{{ route('admin.posts.update', $item->id) }}" enctype="multipart/form-data">
               @method('put')
               @csrf
               <div class="form-group">
                   <label for="name">Название</label>
                   <input type="text" name="name" id="name" value="{{ $item->name }}" class="form-control">
               </div>

               {{--Post image--}}
               <div class="row">
                   <div class="col-md-6">
                       <div class="form-group">
                           <label for="image">Изображение статьи</label>
                           <input type="file" name="image" id="image" class="form-control" >
                       </div>
                   </div>
                   <div class="col-md-4">
                       <div class="form-group">
                           @if($item->image)
                               <img style="max-width: 118px" src="{{ $imagePath }}/{{ $item->image }}" alt="">
                           @else
                               {{--no-image.jpg--}}
                               <img style="max-width: 118px" src="{{ $imagesServe }}/page_no_image.jpg" alt="">
                           @endif
                       </div>
                   </div>

                   @if(isset($item->image))
                       <div class="col-md-2">
                           <label for="delete_image">Удалить изображение</label>
                           <input type="checkbox" id="delete_image" name="delete_image[{{ $item->image }}]">
                       </div>
                   @endif
               </div><!--row-->
               {{--End Post image--}}



               <div class="form-group">
                   <label for="desc">Краткое описание</label>
                   <textarea name="desc" id="desc" cols="30" rows="5" class="form-control">{{ $item->desc }}</textarea>
               </div>

               <label for="text">Текст</label>
               <textarea id="text" name="text">{{ $item->text }}</textarea>
               <script>
                   var options = {
                       filebrowserImageBrowseUrl: '/laravel-filemanager?type=Images',
                       filebrowserImageUploadUrl: '/laravel-filemanager/upload?type=Images&_token=',
                       filebrowserBrowseUrl: '/laravel-filemanager?type=Files',
                       filebrowserUploadUrl: '/laravel-filemanager/upload?type=Files&_token='
                   };

                   CKEDITOR.replace("text", options);
               </script>

               <div class="form-group">
                   <label for="tags">Теги</label>
                   <select name="tags[]" id="tags" class="select2"  multiple="multiple">
                       @foreach($tags as $tag)
                           @if($item->tags()->where('tag_id', $tag->id)->exists())
                               <option selected value="{{ $tag->id }}">{{ $tag->name }}</option>
                           @else
                               <option  value="{{ $tag->id }}">{{ $tag->name }}</option>
                           @endif
                       @endforeach
                   </select>
               </div>

               <div class="form-group">
                   <label for="categories">Категории</label>
                   <select name="categories[]" id="categories" class="select2"   multiple="multiple">
                       <option value="{{ null }}">Родительский категорий</option>
                       {{ selectPostEditNestedCategories($categories, $item->id) }}
                   </select>
               </div>

               <div class="row">
                   <div class="col-md-6">
                       <label for="meta_desc">Мета описание</label>
                       <textarea name="meta_desc" id="meta_desc" cols="15" rows="5" class="form-control">{{ $item->meta_desc }}</textarea>
                   </div>
                   <div class="col-md-6">
                       <label for="keywords">Клчевые слова</label>
                       <textarea name="keywords" id="keywords" cols="15" rows="5" class="form-control">{{ $item->keywords }}</textarea>
                   </div>
               </div><!--row-->
               <br>


               <div class="form-group">
                   <button type="submit" class="btn btn-primary">Обновить</button>
               </div>
           </form>
        </div>
    </div>
@endsection

@section('scripts')
<script>
$(document).ready(function () {
    //Add dynamic images
    $(".add-more").click(function(){
        var html = $(".copy").html();
        $(".after-add-more").after(html);
    });

    $("body").on("click",".remove",function(){
        $(this).parents(".control-group").remove();
    });

    //Add dynamic images end
    
    /*Select2*/
    $(".select2").each(function () {
        $(this).select2({
            theme: 'bootstrap4',
            width: 'style',
            placeholder: $(this).attr('placeholder'),
            allowClear: Boolean($(this).data('allow-clear')),
        });
    });
    /*Select2 end*/
});
</script>
@endsection
