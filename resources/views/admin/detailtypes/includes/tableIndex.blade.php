@foreach($tree as $key => $value)
    @php $dash = ($value['parent_id'] == 0) ? '' : str_repeat('-', $r) .' '; @endphp
    <tr>
        <td>{{ $dash }}{{ $value['name'] }}</td>
        <td>{{ isset($value['path']) ? $value['path'] : '' }}</td>
        <td>
            <a href="{{ route('admin.detailtypes.edit', $value['id']) }}" class="badge badge-primary">Редактировать</a>
            <form method="post" action="{{ route('admin.detailtypes.destroy', $value['id']) }}" class="d-inline">
                @method('delete')
                {{ csrf_field() }}
                <button type="submit" class="badge badge-danger">Удалить</button>
            </form>
        </td>
    </tr>
    @if($value['parent_id'] == $p)
        <?php $r = 0; ?>
    @endif

    @if(isset($value->children))
        @include('admin.detailtypes.includes.tableIndex', ['tree' => $value->children, 'r' => $r+1, $p => $value['parent_id']])
    @endif
@endforeach


