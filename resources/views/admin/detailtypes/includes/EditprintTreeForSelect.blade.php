
@foreach($tree as $key => $item)
    <?php $dash = ($item['parent_id'] == 0) ? '' : str_repeat('-', $r) .' '; ?>
    @if($post_id == $item['id'])
        @continue
    @endif

    <?php $title = $item['name']; ?>

    @if($post->parent_id == $item['id'])

        <option  selected="selected" value="{{ $item['id'] }}">{{ $dash }}{{ $title }}</option>
    @else
        <option value="{{ $item['id'] }}">{{ $dash }}{{ $title }}</option>
    @endif

    @if($item['parent_id'] == $p)
        <?php $r = 0; ?>
    @endif

    @if(isset($item['_children']))
        @include('admin.detailtypes.includes.EditprintTreeForSelect', ['tree' => $item['_children'], 'r' => $r+1, $p => $item['parent_id'], 'post_id' => $post_id])
    @endif
@endforeach


