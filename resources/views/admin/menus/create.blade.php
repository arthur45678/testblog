@extends('layouts.admin')

@section('styles')

@endsection

@section('sidebar')
    @include('admin.includes.sidebar')
@endsection

@section('content')
    @include('admin.includes.info-box')

    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">{{ $title }}</h6>
        </div>
        <div class="card-body">



            <form method="post" action="{{ route('admin.menus.store') }}">
                {{ csrf_field() }}

                <div class="form-group">
                    <label for="parent_id">Порядок вложенности</label>
                    <select name="parent_id" id="parent_id" class="select2">
                        <option value="{{ Null }}">Родительский пункт в меню</option>
                        @php  selectNested($menuitems,  $r = 0, $p = null) @endphp
                    </select>
                </div>

                <div class="form-group">
                    <label for="name">Название</label>
                    <input type="text" name="name" id="name" value="{{ old('name') }}" class="form-control">
                </div>

                <div class="form-group">
                    <label for="path">Ссылка (Можете выбрать что-то из меню или добавит ссылку)</label>
                    <input type="url" name="path" id="path" value="{{ old('path') }}" class="form-control">
                </div>
                <div class="form-group">
                    <label for="path">Введите код иконки Font awesome (Например: fa_icon_code)</label>
                    <input type="text" name="fa_icon_code" id="fa_icon_code" value="{{ old('fa_icon_code') }}" class="form-control">
                </div>


                <div class="panel-group" id="accordion">
                    {{--Item--}}
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <a class="panel-title" data-toggle="collapse" data-parent="#accordion"
                               href="#posts">
                                <label for="posts">
                                    <input type="radio"
                                           name="menu_type"
                                           id="posts"
                                           value="posts">
                                    Раздел последные 15 статьи:
                                </label>
                            </a>
                        </div>
                        <div
                            id="posts"
                            class="panel-collapse collapse">
                            <div class="panel-body">
                                <div class="container-fluid1">
                                    @if(isset($data['posts']) && count($data['posts']) > 0)
                                        <div class="row">
                                            <div class="col-md-8  cat_sel" id="cat">
                                                <select name="" class="form-control" id="">
                                                    @foreach($data['posts'] as $item)
                                                        <option value="{{ route('posts.show',$item->slug) }}">{{ $item->name }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="col-md-4">
                                                <button class="item-name btn btn-primary">Выбрать</button>
                                            </div>
                                        </div>
                                    @else
                                        <p>Нет пунктов</p>
                                    @endif
                                </div>
                            </div>{{--panel-body--}}
                        </div>
                    </div>{{--End item--}}

                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <a class="panel-title" data-toggle="collapse" data-parent="#accordion"
                               href="#product">
                                <label for="product">
                                    <input type="radio"
                                           name="menu_type"
                                           id="product"
                                           value="product">
                                    Раздел последные 15 продукты:
                                </label>
                            </a>
                        </div>
                        <div
                            id="product"
                            class="panel-collapse collapse">
                            <div class="panel-body">
                                <div class="container-fluid1">
                                    @if(isset($data['product']) && count($data['product']) > 0)
                                        <div class="row">
                                            <div class="col-md-8  cat_sel" id="cat">
                                                <select name="" class="form-control" id="">
                                                    @foreach($data['product'] as $item)
                                                        <option value="{{ route('shop.show',$item->slug) }}">{{ $item->name }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="col-md-4">
                                                <button class="item-name btn btn-primary">Выбрать</button>
                                            </div>
                                        </div>
                                    @else
                                        <p>Нет пунктов</p>
                                    @endif
                                </div>
                            </div>{{--panel-body--}}
                        </div>
                    </div>{{--End item--}}


                    {{--Item--}}
                    <div class="panel panel-default">
                        {{--Item--}}
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <a class="panel-title" data-toggle="collapse" data-parent="#accordion"
                                   href="#heading_type_pages">
                                    <label for="type_pages">
                                        <input type="radio"
                                               name="menu_type"
                                               id="type_pages"
                                               value="type_pages">
                                        Страницы:
                                    </label>
                                </a>
                            </div>
                            <div
                                id="heading_type_pages"
                                class="panel-collapse collapse">
                                <div class="panel-body">
                                    <div class="container-fluid1">

                                        <div class="row">
                                            <div class="col-md-8  cat_sel" id="cat">
                                                <select name="" class="form-control" id="">
                                                    <option value="{{ route('contactUS') }}">Контакт
                                                    <option value="{{ route('aboutUs') }}">О компании
                                                    <option value="{{ route('posts.index') }}">Все статьи
                                                    <option value="{{ route('shop.index') }}">Все продукты
                                                    <option value="{{ route('shipping') }}">Доставка
                                                    <option value="{{ route('payments') }}">Оплата
                                                </select>
                                            </div>
                                            <div class="col-md-4">
                                                <button class="item-name btn btn-primary">Выбрать</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>{{--panel-body--}}
                            </div>
                        </div>{{--End item--}}
                    </div>

                </div>

                <div class="form-group">
                    <button type="submit" class="btn btn-primary">Добавить</button>
                </div>

            </form>
        </div>
    </div>
@endsection

@section('scripts')
<script>
$(document).ready(function () {
    $(".select2").each(function () {
        $(this).select2({
            theme: 'bootstrap4',
            width: 'style',
            placeholder: $(this).attr('placeholder'),
            allowClear: Boolean($(this).data('allow-clear')),
        });



        // Sa accordioni hamara
        $(".panel-title").click(function() {
            $(this).children().children().prop("checked", true);
        });


        /**
         * @ntrac hodvaxci, kam selecti anun@ talisa menyuyi anvan vernagir
         * @constructor
         */
        function titleSelectedItemName(selected_item_name) {
            // remove symbol '-' in name
            var name = selected_item_name.replace(/[/-]/g, '');
            name = name.trim();
            $('input[name="name"]').val(name);
        }


        function pathSelectItemPath(selected_item_path) {
            // path
            name = selected_item_path.trim();
            $('input[name="path"]').val(name);
        }

        $('.item-name').on('click', function (e) {
            e.preventDefault();
            selected_item_name = $(this).closest('div.panel-body').find('select option:selected').text();
            titleSelectedItemName(selected_item_name);

            selected_item_path = $(this).closest('div.panel-body').find('select option:selected').val();
            pathSelectItemPath(selected_item_path)
        });
    });
});
</script>
@endsection
