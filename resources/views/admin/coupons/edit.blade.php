@extends('layouts.admin')

@section('styles')

@endsection

@section('sidebar')
    @include('admin.includes.sidebar')
@endsection

@section('content')
    @include('admin.includes.info-box')

    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">{{ $title }}</h6>
        </div>
        <div class="card-body">
           <form method="post" action="{{ route('admin.coupons.update', $item->id) }}">
               @method('put')
               @csrf
               <div class="form-group">
                   <label for="code">Код купона</label>
                   <input type="text" name="code" id="code" value="{{ $item->code }}" class="form-control">
               </div>

               <div class="form-group">
                   <label for="type">Тип купона (скидка)</label>
                   <select name="type" id="type" class="form-control">
                       <option {{ $item->type == 'fixed' ? 'selected' : '' }} value="{{ $item->type }}">Фиксированный</option>
                       <option {{ $item->type == 'percent' ? 'selected' : '' }} value="{{ $item->type }}">Процент</option>
                   </select>
               </div>

               <div class="form-group" id="value-wrap">
                   <label for="value">Значение (Введите в цыфрах)</label>
                   <input type="number" name="value" id="value" value="{{ $item->value }}" class="form-control">
               </div>

               <div class="form-group" id="percent_off-wrap">
                   <label for="percent_off">Процент (Введите в цыфрах)</label>
                   <input type="number" name="percent_off" id="percent_off" value="{{ $item->percent_off  }}" class="form-control">
               </div>

               <div class="form-group">
                   <button type="submit" class="btn btn-primary">Обновить</button>
               </div>
           </form>
        </div>
    </div>
@endsection

@section('scripts')
<script>
$(document).ready(function () {
    setInterval(function(){
        if($('#type').val() === 'fixed'){
            $('#percent_off-wrap').hide();
            $('#value-wrap').show(300);

        }

        if($('#type').val() === 'percent'){
            $('#percent_off-wrap').show(300);
            $('#value-wrap').hide();
        }
    }, 100);
});
</script>
@endsection
