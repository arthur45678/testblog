@extends('layouts.admin')

@section('styles')

@endsection

@section('sidebar')
    @include('admin.includes.sidebar')
@endsection

@section('content')
    @include('admin.includes.info-box')

    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">{{ $title }}</h6>
        </div>
        <div class="card-body">
            <form method="post" action="{{ route('admin.cargenerations.store') }}">
                {{ csrf_field() }}

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="carmodel_id">Производитель</label>
                            <select name="carbrand_id" id="carbrand_id" class="form-control select2">
                                @foreach($carbrands as $key => $value)
                                    <option value="">Выберите призводителья</option>
                                    <option value="{{$key}}">{{$value}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <label for="carmodel_id">Модель</label>
                        <select name="carmodel_id" id="carmodel_id" class="form-control select2"></select>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="name">Поколение</label>
                            <input value="{{ old('name') }}" type="text" name="name" id="name" class="form-control">
                        </div>
                    </div>
                </div><!--row-->

                <div class="form-group">
                    <button type="submit" class="btn btn-primary">Добавить</button>
                </div>
            </form>
        </div>
    </div>
@endsection

@section('scripts')
<script>
$(document).ready(function () {
    /*Select2*/
    $(".select2").each(function () {
        $(this).select2({
            theme: 'bootstrap4',
            width: 'style',
            placeholder: $(this).attr('placeholder'),
            allowClear: Boolean($(this).data('allow-clear')),
        });
    });
    /*Select2 end*/

    $('#carbrand_id').change(function(){

        var carbrand_id = $(this).val();
        if(carbrand_id){
            $.ajax({
                type:"GET",
                url:"{{url('cargenerations/get-carmodels-list')}}?carbrand_id="+carbrand_id,
                success:function(res){
                    if(res){
                        $("#carmodel_id").empty();
                        $("#carmodel_id").append('<option>Select</option>');
                        $.each(res,function(key,value){
                            $("#carmodel_id").append('<option value="'+key+'">'+value+'</option>');
                        });

                    }else{
                        $("#carmodel_id").empty();
                    }
                }
            });
        }else{
            $("#carmodel_id").empty();
            $("#city").empty();
        }
    });

});

</script>
@endsection
