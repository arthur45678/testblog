@extends('layouts.admin')

@section('styles')

@endsection

@section('sidebar')
    @include('admin.includes.sidebar')
@endsection

@section('content')
    @include('admin.includes.info-box')

    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">{{ $title }}</h6>
        </div>
        <div class="card-body">



            <form method="post" action="{{ route('admin.categories.store') }}">
                {{ csrf_field() }}

                <div class="form-group">
                    <select name="parent_id" id="parent_id" class="select2">
                        <option value="{{ Null }}">Родительский пункт в меню</option>
                        @php  selectNested($menuitems,  $r = 0, $p = null) @endphp
                    </select>
                </div>

                <div class="form-group">
                    <label for="name">Название</label>
                    <input type="text" name="name" id="name" value="{{ old('name') }}" class="form-control">
                </div>

                <div class="form-group">
                    <button type="submit" class="btn btn-primary">Добавить</button>
                </div>
            </form>
        </div>
    </div>
@endsection

@section('scripts')
<script>
$(document).ready(function () {
    $(".select2").each(function () {
        $(this).select2({
            theme: 'bootstrap4',
            width: 'style',
            placeholder: $(this).attr('placeholder'),
            allowClear: Boolean($(this).data('allow-clear')),
        });
    });
});
</script>
@endsection
