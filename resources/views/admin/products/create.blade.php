@extends('layouts.admin')

@section('styles')

@endsection

@section('sidebar')
    @include('admin.includes.sidebar')
@endsection

@section('content')
    @include('admin.includes.info-box')

    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">{{ $title }}</h6>
        </div>
        <div class="card-body">
            <form method="post" action="{{ route('admin.products.store') }}" enctype="multipart/form-data">
                {{ csrf_field() }}


                <div class="form-group">
                    <label for="name">Названеи продукта</label>
                    <input type="text" id="name" name="name" class="form-control">
                </div>

                <div class="form-group">
                    <label for="brand_id">Производитель</label>
                    <select name="carbrand_id" id="brand_id" class="form-control select2">
                        <option value="">Выберите призводителья</option>
                        @foreach($carbrands as $key => $value)
                            <option value="{{$key}}">{{$value}}</option>
                        @endforeach
                    </select>
                </div>

                <div class="form-group">
                    <label for="short_desc">Короткое описание</label>
                    <input type="text" name="short_desc" id="short_desc" class="form-control">
                </div>

                <label for="description">Текст</label>
                <textarea id="description" name="description" ></textarea>
                <script>
                    var options = {
                        filebrowserImageBrowseUrl: '/laravel-filemanager?type=Images',
                        filebrowserImageUploadUrl: '/laravel-filemanager/upload?type=Images&_token=',
                        filebrowserBrowseUrl: '/laravel-filemanager?type=Files',
                        filebrowserUploadUrl: '/laravel-filemanager/upload?type=Files&_token='
                    };

                    CKEDITOR.replace("description", options);
                </script>

                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <h4>Применим</h4>
                        </div>
                    </div>
                </div><!--row-->


                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="carbrand_id">Производитель</label>
                            <select name="carbrand" id="carbrand_id" class="form-control select2">
                                <option value="">Выберите призводителья</option>
                                @foreach($carbrands as $key => $value)
                                    <option value="{{$key}}">{{$value}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <label for="carmodel_id">Модель</label>
                        <select name="carmodel[]" id="carmodel_id" class="form-control select2" multiple="multiple"></select>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="name">Поколение</label>
                            <select name="cargeneration[]" id="cargeneration" class="form-control select2" multiple="multiple"></select>
                        </div>
                    </div>
                </div><!--row-->

                <div class="form-group">
                    <label for="detailtypes">Тип детали</label>
                    <select name="detailtypes[]" id="detailtypes" class="select2" multiple="multiple">
                        @include('admin.products.includes.printTreeForSelect', ['tree' => $tree,  $r = 0, $p = null])
                    </select>
                </div>

                <div class="form-group">

                    <div class="form-check-inline">
                        <label class="form-check-label">
                            <input type="radio" class="form-check-input" name="is_original" value="1" checked>Оригинал
                        </label>
                    </div>
                    <div class="form-check-inline">
                        <label class="form-check-label">
                            <input type="radio" class="form-check-input" name="is_original" value="0">Не оригинал
                        </label>
                    </div>

                </div>

                <div class="form-group">
                    <div class="form-check-inline">
                        Состояние:&nbsp;
                        <label class="form-check-label">
                            <input type="radio" class="form-check-input" name="new_product" value="yes" checked> Новое
                        </label>
                    </div>
                    <div class="form-check-inline">
                        <label class="form-check-label">
                            <input type="radio" class="form-check-input" name="new_product" value="no">Бу
                        </label>
                    </div>
                </div>

               <div class="row">
                   <div class="col-md-4">
                       <div class="form-group">
                           <label for="price">Цена В AMD</label>
                           <input type="number" name="price" id="price" class="form-control" value="{{ old('price') }}">
                       </div>
                   </div>

                   <div class="col-md-4">
                       <div class="form-group">
                           <label for="price">Количество</label>
                           <input type="number" name="quantity" id="quantity" class="form-control" value="{{ old('quantity') }}">
                       </div>
                   </div>
               </div><!--row-->


                <div class="form-group">
                    <label for="image">Главное изображение продукта</label>
                    <input type="file" name="image" id="image" class="form-control" >
                </div>



                {{--Add dynamic images fields--}}
                <div class="form-group after-add-more">
                    <div class="input-group-btn">
                        <button class="btn btn-success add-more" type="button"><i class="glyphicon glyphicon-plus"></i> Добавиь изображение</button>
                    </div>
                </div>


                <!-- Copy Fields -->
                <div class="form-group">
                    <div class="copy d-none">
                        <div class="control-group input-group" style="margin-top:10px">
                            <input name="images_create[]" id="images" type="file" class="form-control filestyle" data-buttonName="btn-primary">
                            <div class="input-group-btn">
                                <button class="btn btn-danger remove" type="button"><i class="glyphicon glyphicon-remove"></i> Remove</button>
                            </div>
                        </div>
                    </div>{{--Add dynamic images fields--}}
                </div>


                <div class="form-group">
                    <label for="sku">Артикул продукта</label>
                    <input type="text" name="sku" id="" class="form-control">
                </div>

                <div class="row">
                    <div class="col-md-3">
                        <label for="color">Цвет</label>
                        <input type="text" name="color" id="color" class="form-control">
                    </div>
                    <div class="col-md-3">
                        <label for="height">Высота (см)</label>
                        <input type="text" name="height" id="height" class="form-control">
                    </div>
                    <div class="col-md-3">
                        <label for="width">Ширина (см)</label>
                        <input type="text" name="width" id="width" class="form-control">
                    </div>

                    <div class="col-md-3">
                        <label for="weight">Вес в кг</label>
                        <input type="text" name="weight" id="weight" class="form-control">
                    </div>
                </div><!--row-->

                <div class="form-group">

                    <div class="form-check-inline">
                        <label class="form-check-label">
                            <input type="radio" class="form-check-input" name="featured" value="1" checked>Есть в наличии
                        </label>
                    </div>
                    <div class="form-check-inline">
                        <label class="form-check-label">
                            <input type="radio" class="form-check-input" name="featured" value="0">Нет в наличии
                        </label>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <label for="meta_keywords">Ключевые слова</label>
                        <textarea class="form-control" name="meta_keywords" id="meta_keywords" cols="30" rows="4"></textarea>
                    </div>
                    <div class="col-md-6">
                        <label for="meta_description">Мета описание</label>
                        <textarea class="form-control" name="meta_description" id="meta_description" cols="30" rows="4"></textarea>
                    </div>
                </div><!--row-->



                <div class="form-group">
                    <button type="submit" class="btn btn-primary">Добавить</button>
                </div>


            </form>
        </div>
    </div>
@endsection


@section('scripts')
    <script>
        $(document).ready(function () {
            //Add dynamic images
            $(".add-more").click(function(){
                var html = $(".copy").html();
                $(".after-add-more").after(html);
            });

            $("body").on("click",".remove",function(){
                $(this).parents(".control-group").remove();
            });

            //Add dynamic images end

            /*Select2*/
            $(".select2").each(function () {
                $(this).select2({
                    theme: 'bootstrap4',
                    width: 'style',
                    placeholder: $(this).attr('placeholder'),
                    allowClear: Boolean($(this).data('allow-clear')),
                });
            });
            /*Select2 end*/

            $('#carbrand_id').change(function(){
                var carbrand_id = $(this).val();
                if(carbrand_id){
                    $.ajax({
                        type:"GET",
                        url:"{{url('cargenerations/get-carmodels-list')}}?carbrand_id="+carbrand_id,
                        success:function(res){
                            if(res){
                             //   $("#carmodel_id").empty();
                                //$("#carmodel_id").append('<option>Select</option>');
                                $.each(res,function(key,value){
                                    $("#carmodel_id").append('<option value="'+key+'">'+value+'</option>');
                                });

                            }else{
                                $("#carmodel_id").empty();
                            }
                        }
                    });
                }else{
                    $("#carmodel_id").empty();
                    $("#city").empty();
                }
            });
            $('#carmodel_id').on('change',function(){
                var carmodelID = $(this).val();
                if(carmodelID){
                    $.ajax({
                        type:"GET",
                        url:"{{url('cargenerations/get-cargenerations-list')}}?carmodel_id="+carmodelID,
                        success:function(res){
                            if(res){
                              //  $("#cargeneration").empty();
                                $.each(res,function(key,value){
                                    $("#cargeneration").append('<option value="'+key+'">'+value+'</option>');
                                });

                            }else{
                                $("#cargeneration").empty();
                            }
                        }
                    });
                }else{
                    $("#cargeneration").empty();
                }

            });
        });

    </script>
@endsection
