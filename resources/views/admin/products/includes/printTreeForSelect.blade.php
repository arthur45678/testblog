@foreach($tree as $key => $value)
    <?php $dash = ($value['parent_id'] == 0) ? '' : str_repeat('-', $r) .' '; ?>
    <option value="{{ $value['id'] }}">{{ $dash }}{{ $value['name'] }}</option>

    @if($value['parent_id'] == $p)
        <?php $r = 0; ?>
    @endif

    @if(isset($value['_children']))
        @include('admin.products.includes.printTreeForSelect', ['tree' => $value['_children'], 'r' => $r+1, $p => $value['parent_id']])
    @endif

@endforeach

