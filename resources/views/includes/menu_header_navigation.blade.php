@if(isset($item['_children']))

    @if($firstParentElement == true)
        <li class="shrift-normal">


            <a  href="{{ route('categoryPosts',['id' => $item['id']])  }}" >{{ $item['title'] }}</a>

            <ul>
                @foreach($item['_children'] as $item)
                    @include('includes.menu_header_navigation', ['firstParentElement' => false])
                @endforeach
            </ul>

        </li>
    @else
        {{--<li class="shrift-normal">--}}
            {{--<a  href="{{ $item['path'] }}">{{ $item['title'] }}</a>--}}
            {{--<ul >--}}
                {{--@foreach($item['_children'] as $item)--}}
                    {{--@include('site.includes.menu_header_navigation')--}}
                {{--@endforeach--}}
            {{--</ul>--}}
        {{--</li>--}}
        <li class="shrift-normal">

            <a  href="{{ route('categoryPosts',['id' => $item['id']]) }}"  id="navbarDropdownMenuLink2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">{{ $item['title'] }}</a>

                <ul>
                    @foreach($item['_children'] as $item)
                        @include('includes.menu_header_navigation', ['firstParentElement' => false])
                    @endforeach
                </ul>

        </li>
    @endif
@else
    <li class="shrift-normal has-sub">
        <a  href="{{ route('categoryPosts',['id' => $item['id']]) }}">{{ $item['title'] }}</a>
    </li>
@endif

